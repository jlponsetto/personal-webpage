""" Scripts """
from decimal import Decimal, DecimalException, getcontext
import importlib.metadata
import sqlite3
from subprocess import check_output, STDOUT
from xml.etree import ElementTree

import requests

from fws.strings import LNK, STR


def gpf_python(number):
    """
    Calculate greatest prime factor using backend python

    Args:
        number: user-entered number for which to calculate GPF
    Returns:
        GPF calculation
    """
    try:
        number = float(number)
        if number == int(number) and 1 < number < 1e15:
            gpf = number
            factor = 2
            # find prime factors less than sqrt(number)
            while factor * factor <= number:
                # any factor here will be prime
                if not number % factor:
                    gpf = factor
                    number = number / factor
                else:
                    factor += 1
            return 'Greatest prime factor: ' + str(int(max(number, gpf)))
        if number == int(number) and number >= 1e15:
            return STR['PY_BIG']
        return STR['PY_ERR']
    except (NameError, OverflowError, SyntaxError, ValueError):
        return STR['PY_ERR']


def gpf_wolfram(number):
    """
    Calculate greatest prime factor using Wolfram Alpha API

    Args:
        number: user-entered number for which to calculate GPF
    Returns:
        GPF calculation
    """
    try:
        getcontext().Emax = 99
        num_flt = 1 * Decimal(number)  # test against Emax to protect server
        if num_flt == int(num_flt) and num_flt > 1:
            query = 'largest prime factor of ' + str(int(num_flt))
            params = {'input': query, 'appid': STR['APP_ID']}
            xml = requests.get(STR['BASE_URL'], params=params, timeout=10).text
            tree = ElementTree.fromstring(xml)
            for pod in tree.iter('pod'):
                if pod.get('title') == 'Result':
                    for answer in pod.iter('plaintext'):
                        return 'Greatest prime factor: ' + answer.text
            return STR['WA_UNEXPECTED']
        return STR['WA_ERROR']
    except (DecimalException, NameError, OverflowError, SyntaxError, ValueError):
        return STR['WA_ERROR']


def check_versions():
    """
    Check server information

    Returns:
        list of server parameters
    """
    def get_aws_info():
        """
        Get EC2 information

        Returns:
            ec2: EC2 instance type
            vol: storage volume size and type
        """
        ec2 = check_output(['ec2metadata', '--instance-type']).decode().strip()
        info = check_output(['df', '-h', '/dev/nvme0n1p1']).decode()
        vol_size = info.split('nvme0n1p1')[1].split('G')[0].strip()
        vol = vol_size + ' GB gp3'
        return ec2, vol

    def get_cpu_info():
        """
        Get CPU information

        Returns:
            info: CPU model name
        """
        cpu_file = check_output('lscpu').decode()
        vendor = cpu_file.split('Vendor ID:')[1].split('\n')[0].strip()
        model = cpu_file.split('Model name:')[1].split('\n')[0].strip()
        info = vendor + ' ' + model
        return info

    def get_ram_info():
        """
        Get RAM information

        Returns:
            info: RAM size
        """
        with open('/proc/meminfo', 'r', encoding='utf-8') as rfile:
            for line in rfile:
                if 'MemTotal' in line:
                    info = line.split(' k')[0].split('l:')[1].strip()
                    info = str(round(float(info) / 1e6, 1))
                    return info
        return '[not detected]'

    def get_tls_info():
        """
        Get TLS information

        Returns:
            TLS version
        """
        trace = str(requests.get(LNK['cdn-cgi'], timeout=5).text)
        tls_idx1 = trace.find(STR['TRACE_TLS'])
        tls_idx2 = trace[tls_idx1:].find('\n')
        return trace[tls_idx1 + len(STR['TRACE_TLS']):tls_idx1 + tls_idx2]

    v_ec2, v_vol = get_aws_info()
    v_cpu = get_cpu_info()
    v_ram = get_ram_info()
    v_ngx = check_output(['nginx', '-v'], stderr=STDOUT).decode().split('/')[1].strip()
    v_uwg = check_output(['uwsgi', '--version']).decode().split('-', 1)[0].strip()
    v_fsk = importlib.metadata.version('flask')
    v_pyt = check_output(['python3', '-V']).decode().split(' ')[1].strip()
    v_sql = sqlite3.sqlite_version
    v_lin = check_output(['uname', '-r']).decode().split('-', 1)[0]
    v_tls = get_tls_info()
    return [v_ec2, v_vol, v_cpu, v_ram, v_ngx, v_uwg, v_fsk, v_pyt, v_sql, v_lin, v_tls]


def scan_ports(ip_in):
    """
    Run port scan

    Args:
        ip_in: user-entered ip or domain to scan
    Returns:
        result of Nmap scan
    """
    # if '//' in ip_in:  # re-enable if AWS supports IPv6 better someday...
    #     ip_in = ip_in[ip_in.index('//') + 2:len(ip_in)]
    # command = ['nmap', '-F', ip_in]
    # if ':' in ip_in:
    #     command.insert(1, '-6')
    command = ['nmap', '-6', '-F', ip_in]
    ports = check_output(command, stderr=STDOUT).decode('utf-8')
    if 'SERVICE' in ports:
        return ports[ports.index('PORT'):ports.index('Nmap done')]
    if 'All 100 scanned ports' in ports:
        return 'All scanned ports are filtered (blocked).'
    if 'Host seems down' in ports:
        return 'Host did not respond.'
    if 'Failed to resolve' in ports:
        return 'Failed to resolve input address.'
    return 'Invalid input!'
