""" Flask init file """
import json
import sqlite3

from flask import Flask, g, jsonify, render_template, request
from werkzeug.exceptions import BadRequest, UnsupportedMediaType

from fws.scripts import gpf_python, gpf_wolfram, check_versions, scan_ports
from fws.strings import LNK, STR

app = Flask(__name__)
app.config['DEBUG'] = True


def get_db():
    """
    Connect to database

    Returns:
        dtb: database object
    """
    dtb = getattr(g, '_database', None)
    if dtb is None:
        dtb = g._database = sqlite3.connect(STR['DATABASE'])
    return dtb


@app.teardown_appcontext
def close_connection(_):
    """ Close database connection """
    dtb = getattr(g, '_database', None)
    if dtb is not None:
        dtb.close()


@app.route('/')
def index():
    """ Render home page """
    return render_template('index.html')


@app.route('/euler/')
def euler():
    """ Render /euler/ """
    return render_template('euler.html',
                           LNK=LNK)


@app.route('/ham/')
def ham():
    """ Render /ham/ """
    return render_template('ham.html',
                           LNK=LNK)


@app.route('/hobbies/')
def hobbies():
    """ Render /hobbies/ """
    return render_template('hobbies.html')


@app.route('/life/')
def life():
    """ Render /life/ """
    return render_template('life.html',
                           question=STR['QUESTION'],
                           choice_1=STR['CHOICE_1'],
                           choice_2=STR['CHOICE_2'],
                           choice_3=STR['CHOICE_3'],
                           LNK=LNK)


@app.route('/nand2tetris/')
def nand2tetris():
    """ Render /nand2tetris/ """
    return render_template('nand2tetris.html',
                           LNK=LNK)


@app.route('/plasma/')
def plasma():
    """ Render /plasma/ """
    return render_template('plasma.html',
                           LNK=LNK)


@app.route('/primes/', methods=['GET', 'POST'])
def primes():
    """ Render /primes/ """
    try:
        num_in = request.get_json()['pyprime']
        gpf = gpf_python(num_in)
        response_dict = {'server_response': gpf}
        return jsonify(response_dict)
    except (BadRequest, UnsupportedMediaType, json.JSONDecodeError, KeyError, TypeError):
        pass
    try:
        num_in = request.get_json()['waprime']
        wgpf = gpf_wolfram(num_in)
        response_dict = {'server_response': wgpf}
        return jsonify(response_dict)
    except (BadRequest, UnsupportedMediaType, json.JSONDecodeError, KeyError, TypeError):
        return render_template('primes.html')


@app.route('/results/', methods=['GET', 'POST'])
def results():
    """ Render /results/ """
    dtb = get_db()
    cur = dtb.cursor()
    new_pct = choice_pct = {}
    vote_totals = cur.execute("SELECT votes FROM 'wall_choice';").fetchall()
    choice_votes = [vote_totals[0][0], vote_totals[1][0], vote_totals[2][0]]
    try:
        selected_choice = int(request.form['choices'])
        choice_votes[selected_choice - 1] = choice_votes[selected_choice - 1] + 1
        cur.execute("UPDATE wall_choice SET votes = ? WHERE id = ?",
                    (choice_votes[selected_choice - 1], selected_choice))
        sum_total = sum(choice_votes)
        for ind_c in range(3):
            # calculate new percentages
            new_pct[ind_c] = 100 * choice_votes[ind_c] / sum_total
            # write new values here
            cur.execute("UPDATE wall_choice SET vote_pct = ? WHERE id = ?",
                        (new_pct[ind_c], ind_c + 1))
        dtb.commit()
    except KeyError:
        pass
    vote_pct = cur.execute("SELECT vote_pct FROM 'wall_choice';").fetchall()
    for ind_c in range(3):
        choice_pct[ind_c] = vote_pct[ind_c][0]
    return render_template('results.html',
                           question=STR['QUESTION'],
                           choice_1=STR['CHOICE_1'],
                           choice_2=STR['CHOICE_2'],
                           choice_3=STR['CHOICE_3'],
                           choice_1_votes=choice_votes[0],
                           choice_2_votes=choice_votes[1],
                           choice_3_votes=choice_votes[2],
                           choice_1_pct=choice_pct[0],
                           choice_2_pct=choice_pct[1],
                           choice_3_pct=choice_pct[2])


@app.route('/scan/', methods=['GET', 'POST'])
def scan():
    """ Render /scan/ """
    try:
        ip_in = request.get_json()['scan']
        port_result = scan_ports(ip_in)
        response_dict = {'server_response': port_result}
        return jsonify(response_dict)
    except (BadRequest, UnsupportedMediaType, json.JSONDecodeError, KeyError, TypeError):
        return render_template('scan.html')


@app.route('/site/')
def site():
    """ Render /site/ """
    ver_info = check_versions()
    return render_template('site.html',
                           v_ec2=ver_info[0],
                           v_vol=ver_info[1],
                           v_cpu=ver_info[2],
                           v_ram=ver_info[3],
                           v_ngx=ver_info[4],
                           v_uwg=ver_info[5],
                           v_fsk=ver_info[6],
                           v_pyt=ver_info[7],
                           v_sql=ver_info[8],
                           v_lin=ver_info[9],
                           v_tls=ver_info[10])


@app.route('/work/')
def work():
    """ Render /work/ """
    return render_template('work.html',
                           LNK=LNK)
