""" Strings """

STR = {
    'APP_ID': 'GKQK6J-Y94UEWQVWL',
    'BASE_URL': 'http://api.wolframalpha.com/v2/query?',
    'CHOICE_1': 'awesome!',
    'CHOICE_2': 'so awesome',
    'CHOICE_3': 'fantastic',
    'DATABASE': '/home/admin/personal-webpage/fws/db.sqlite3',
    'PY_BIG': ('Take it easy on my hardware! Please enter a positive integer greater '
               'than 1 and less than 10^15 (one quadrillion). If you want to factor a '
               'very large number, try using the Wolfram Alpha API method below.'),
    'PY_ERR': ('Invalid input! Please enter a positive integer greater than '
               '1 and less than 10^15 (one quadrillion).'),
    'QUESTION': 'This site is...',
    'TRACE_TLS': 'tls=TLSv',
    'WA_ERROR': ('Invalid input! Please enter a positive integer greater than '
                 '1, in decimal form.'),
    'WA_UNEXPECTED': ('Wolfram Alpha returned a query result failure. Please try a '
                      'smaller number, or one that is not mistakable as binary.'),
}

LNK = {
    'cdn-cgi': 'https://jlponsetto.com/cdn-cgi/trace',
    'cosmos': 'https://jacobsschool.ucsd.edu/cosmos',
    'ece': 'https://ucsd.edu/catalog/courses/ECE.html',
    'euler': 'https://projecteuler.net',
    'euler_pic': 'https://projecteuler.net/profile/jlponsetto.png',
    'ham': 'https://wireless2.fcc.gov/UlsApp/UlsSearch/license.jsp?licKey=3817395',
    'map': 'https://google.com/maps/d/embed?mid=1j4vw5vQerIUyiaLlaH1GSAx3Z1aEzPMi',
    'nand2tetris': 'https://nand2tetris.org',
    'padilla': 'https://padillalab.pratt.duke.edu',
    'plasma': 'https://en.wikipedia.org/wiki/Plasma_speaker',
    'radic': 'http://phosys.ucsd.edu',
    'scholar': 'https://scholar.google.com/citations?user=URm5uVEAAAAJ',
    'spacex': 'https://spacex.com',
    'thesis': 'https://search.proquest.com/docview/1842657447',
    'zhaowei': 'https://www.zhaowei.us',
}

IPV4 = ['cosmos', 'ece', 'nand2tetris', 'padilla', 'radic', 'scholar', 'thesis', 'zhaowei']
