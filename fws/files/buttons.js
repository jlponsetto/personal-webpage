document.addEventListener('DOMContentLoaded', function() {
    let sInputInfo = 'scan';
    let sPage = '/scan/';
    let sErrorMsg = document.createTextNode('Invalid input!');
    let sWaitMsg = document.createTextNode('Scanning...');
    scanButton(sInputInfo, sPage, sErrorMsg, sWaitMsg);
    let pInputInfo = 'pyprime';
    let pPage = '/primes/';
    let pErrorMsg = document.createTextNode('Error!');
    let pWaitMsg = document.createTextNode('Calculating...');
    scanButton(pInputInfo, pPage, pErrorMsg, pWaitMsg);
    let wInputInfo = 'waprime';
    scanButton(wInputInfo, pPage, pErrorMsg, pWaitMsg);
    let jButton = document.getElementById('jsprime_button');
    if (jButton) {
        let jInput = document.getElementById('jsprime_input');
        let jResult = document.getElementById('jsprime_result');
        jButton.onclick = function() {
            jResult.removeChild(jResult.firstChild);
            jResult.appendChild(pWaitMsg);
            let w = new Worker('/static/js_primes.js');
            w.postMessage(jInput.value);
            w.onmessage = function(event) {
                let jMsg = document.createTextNode(event.data);
                jResult.removeChild(jResult.firstChild);
                jResult.appendChild(jMsg);
            };
            return false;
        };
    }
}, false);

function scanButton(inputInfo, page, errorMsg, waitMsg) {
    let button = document.getElementById(`${inputInfo}_button`);
    let input = document.getElementById(`${inputInfo}_input`);
    let result = document.getElementById(`${inputInfo}_result`);
    if (button) {
        button.onclick = function() {
            result.removeChild(result.firstChild);
            result.appendChild(waitMsg);
            let xhr = new XMLHttpRequest();
            xhr.responseType = 'json';
            xhr.open('POST', page, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            let dataJSON = JSON.stringify({
                [inputInfo]: input.value
            });
            xhr.send(dataJSON);
            xhr.onload = function() {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    let jsonInfo = xhr.response.server_response;
                    let msg = document.createTextNode(jsonInfo);
                    result.removeChild(result.firstChild);
                    result.appendChild(msg);
                } else {
                    result.removeChild(result.firstChild);
                    result.appendChild(errorMsg);
                }
            };
            return false;
        };
    }
}
