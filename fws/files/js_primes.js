function pcalc(n) {
    let number = n.data;
    if (number % 1 === 0 && number > 1 && number < Number.MAX_SAFE_INTEGER) {
        let gpf = number;
        let factor = 2;
        // find prime factors less than sqrt(number)
        while (factor*factor <= number) {
            if (number % factor === 0) {
                // any factor here will be prime
                gpf = factor;
                number = number/factor;
            } else {
                factor += 1;
            }
        }
        postMessage('Greatest prime factor: ' + Math.max(number, gpf));
    } else if (number % 1 === 0 && number >= Number.MAX_SAFE_INTEGER) {
        postMessage('MAX_SAFE_INTEGER exceeded! JavaScript only safely' +
        ' represents integers up to 2^53. If you want to factor' +
        ' a very large number, try using the Wolfram Alpha API method' +
        ' below.');
    } else {
        postMessage('Invalid input! Please enter a positive integer greater' +
        ' than 1 and less than 2^53.');
    }
}
addEventListener('message', pcalc, true);
