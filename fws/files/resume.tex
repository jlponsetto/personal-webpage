\documentclass[letterpaper]{article}
\usepackage{enumitem}
\usepackage[margin=1in]{geometry}
\usepackage[small]{titlesec}
\pagestyle{empty}
\setlength{\parindent}{0pt}
\titlespacing\section{0pt}{12pt}{6pt}

\begin{document}

\begin{center}\large{\textbf{Joseph L. Ponsetto}}\end{center}
[street address] \hfill [phone number] \\
Venice, CA 90291 \hfill [email address] \\
United States of America \hfill www.jlponsetto.com

\section*{SUMMARY}

An energetic, passionate physicist and engineer with proven, vertically-integrated skills and the ability to deliver a product from start to finish. Experienced in full-stack design of photonic systems for optical communication in space and terrestrial applications. Flexible, cooperative, intellectually curious, and a fast learner with the ability to lead by example.

\section*{SKILLS}

\begin{itemize} \itemsep -2pt

\item Hardware design and characterization: free-space and fiber optics, CW and pulsed lasers, optical amplifiers, coherent and IMDD transceivers, navigation systems, accelerometers, gyroscopes, gas sensing, inductive sensing, current sensing, photodiodes, analog front-end design, oscilloscopes, function generators, spectrum analyzers, BERT, DAQ, microscopy

\item Programming languages: Python, MATLAB, Android Linux, C\texttt{++}, C, LabVIEW, JavaScript

\item Design/simulation software: CST, COMSOL, Zemax, Altium, VPIphotonics, NX, Mathematica

\item Cleanroom fabrication: nanoimprint lithography, plasma etching, spin coating, ellipsometry, EBPVD, SEM, FIB, AFM

\item Administrative/OS: Windows, Linux, \LaTeX, Microsoft Office, Google Apps, Adobe Acrobat

\item Leadership: public speaking, written and oral communication, fundraising, group organization

\item Autodidacticism: web development, computer architecture, amateur radio and electronics

\item Language: reading, writing, speaking, and comprehension in English and Spanish

\end{itemize}

\section*{WORK EXPERIENCE}

\vspace{1ex}
\textbf{SpaceX;} Hawthorne, CA \\[1ex]
\textbf{\textit{\hspace*{3mm} Sr. Space Lasers Engineer, 2022 - Present}}

\begin{itemize}[topsep=0pt] \itemsep -2pt
	
\item Designing a state of the art space-based laser communication terminal for Starlink and other ventures, focusing on the fiber amplifier and coherent transceiver

\item Successfully introduced the first fully in-house fiber amplifier, owning the technical product and business outcome from conceptualization through prototyping and qualification, to volume production
	
\end{itemize}

\textbf{\textit{\hspace*{3mm} Space Lasers Engineer, 2019 - 2022}}

\begin{itemize}[topsep=0pt] \itemsep -2pt
	
\item Responsible for the fiber amplifier design on the company's first-ever inter-satellite laser

\item Resolved several inherited hardware reliability problems to ensure mission success
	
\end{itemize}

\vspace{1ex}
\textbf{\textit{\hspace*{3mm} Avionics Test Engineer, 2017 - 2019}}

\begin{itemize}[topsep=0pt] \itemsep -2pt

\item Supported rigorous qualification of new avionics for the world's most advanced rockets and spacecraft

\item Designed and implemented robust acceptance test systems for avionics and sensors, responsible for all aspects of test system hardware and software

\end{itemize}

\vspace{1ex}
\textbf{Electrical Engineering Department, University of California, San Diego;} La Jolla, CA \\[1ex]
\textbf{\textit{\hspace*{3mm} Powell Research Fellow, 2010 - 2016}}

\begin{itemize}[topsep=0pt] \itemsep -2pt

\item Led and published research under Prof. Z. Liu in the areas of super-resolution microscopy, nano-photonics, and plasmonic structures for imaging and lithography applications

\item Designed, fabricated, characterized, and successfully implemented a nano-antenna array within a complex optoelectronic system to achieve super-resolution optical imaging

\item Developed breadth and depth of interdisciplinary experience in experimental optics, fabrication, sensing, signal processing, reconstruction algorithms, and software prototyping

\item Conducted and published research under Prof. S. Radic in the areas of nonlinear fiber optics, optical ADC preprocessing via four-wave mixing, and EDFA transient mitigation

\item Designed, built, and tested many optoelectronic tools: complex free-space optics platforms, custom EDFAs, ultrashort pulse characterizers, and frequency comb generators

\item Lectured, tutored, graded, and created course material for electrical engineering classes as a graduate teaching assistant

\end{itemize}

\vspace{1ex}
\textbf{Physics Department, Boston College;} Chestnut Hill, MA \\[1ex]
\textbf{\textit{\hspace*{3mm} Research Assistant, 2008 - 2010}}

\begin{itemize}[topsep=0pt] \itemsep -2pt

\item Conducted and published research under Prof. W. Padilla on the photonic properties of metamaterials

\end{itemize}

\section*{EDUCATION}

\textbf{University of California, San Diego}, La Jolla, CA \\
Ph.D. Applied Physics (ECE); 2016 \\
M.S. Applied Physics (ECE); 2012 \\
\textbf{Boston College}, Chestnut Hill, MA \\
B.S. Physics, Minors in Mathematics and Hispanic Studies; 2010 \\
Student exchange semester at Pontificia Universidad Cat\'{o}lica; Santiago, Chile

\section*{TEACHING}

\begin{itemize} \itemsep -2pt

\item ECE 25: Introduction to Digital Design

\item ECE 45: Circuits and Systems

\item ECE 101: Linear Systems Fundamentals

\item ECE 109: Engineering Probability and Statistics

\end{itemize}

\section*{LEADERSHIP / AWARDS / CERTIFICATIONS}

\begin{itemize} \itemsep -2pt

\item Invited speaker at Aerospace Corp MRQW, 2021

\item Invited keynote speaker at UCSD PhD recruitment event, 2019

\item Powell Research Fellow, UCSD, 2010 - 2016

\item Vice President of UCSD Departmental Graduate Student Council, 2013 - 2014

\item Department Representative to UCSD Graduate Student Association, 2010 - 2015

\item Micro-MBA Certificate, UCSD, 2014

\item Dean's List, Boston College, 2006 - 2010

\item FCC-licensed amateur radio technician (callsign: KM6DDU), 2016 - Present

\item PADI-licensed Advanced Open Water Diver, 2016 - Present

\item Member IEEE, OSA, SPIE, SPS

\end{itemize}

\section*{SELECTED PUBLICATIONS}

\begin{itemize} \itemsep -2pt

\item J. L. Ponsetto et al., \textit{``Experimental demonstration of localized plasmonic structured illumination microscopy'',} ACS Nano, Vol. 11, pp. 5344-5350 (2017)

\item J. L. Ponsetto and Z. Liu, \textit{``The far-field superlens''} in \textit{``Plasmonics and super resolution imaging'',} Pan Stanford Publishing (2017)

\item W. Wan$^{\dagger}$, J. L. Ponsetto$^{\dagger}$, and Z. Liu, \textit{``Numerical study of hyperlenses for three-dimensional imaging and lithography'',} Opt. Expr., Vol. 23, pp. 18501-18510 (2015) [$^{\dagger}$Equal contribution]

\item J. L. Ponsetto, F. Wei, and Z. Liu, \textit{``Localized plasmon assisted structured illumination microscopy'',} Nanoscale, Vol. 6, pp. 5807-5812 (2014)

\end{itemize}

\end{document}