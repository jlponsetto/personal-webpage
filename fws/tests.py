""" Tests """
import requests

from fws.scripts import gpf_python  # , gpf_wolfram
from fws.strings import IPV4, STR, LNK


def test_gpf():
    """ Check if GPF calculation works """
    t_in = ['test', '-3', '33', '33.2', '33e2', '33e2e', '22e222', '22e2222', '99e99999999']
    t_exp = [STR['PY_ERR'], STR['PY_ERR'], 'Greatest prime factor: 11', STR['PY_ERR'],
             'Greatest prime factor: 11', STR['PY_ERR'], STR['PY_BIG'], STR['PY_ERR'],
             STR['PY_ERR']]
    for ind, val in enumerate(t_in):
        assert gpf_python(val) == t_exp[ind], 'failed GPF of ' + val
    # currently disabled pending AWS / Wolfram IPv6 support
    # assert gpf_wolfram(t_in[4]) == t_exp[4], 'failed Wolfram GPF'


def test_links():
    """ Check if there are dead external links """
    for name, link in LNK.items():
        if name not in IPV4:
            status = requests.get(
                link, headers={'User-Agent': 'Mozilla/5.0'}, timeout=10).status_code
            if status != 200:
                if status == 403:  # e.g. Google scholar has sophisticated anti-scraping
                    print('403 for ' + name)
                else:
                    print('failed link test of ' + name + ' with ' + str(status))


def test_pages():
    """ Check if pages load """
    for page in ['primes', 'results', 'scan', 'site', 'work']:
        link = f'https://jlponsetto.com/{page}/'
        status_code = requests.get(link, timeout=5).status_code
        assert status_code == 200, f'failed {page} test, status code {status_code}'


def run_tests():
    """ Run all tests in this file """
    test_gpf()
    test_pages()
    test_links()
    print('all tests passed')


run_tests()
