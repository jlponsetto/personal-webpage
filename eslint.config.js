const globals = require("globals");
const js = require("@eslint/js");

module.exports = [
    js.configs.recommended,
    {
        languageOptions: {
            globals: {
                ...globals.browser
            }
        },
        rules: {
            "comma-dangle": ["warn", "never"],
            "indent": ["warn", 4],
            "no-extra-semi": ["warn"],
            "no-extra-parens": ["warn", "all"],
            "semi": ["warn", "always"],
            "semi-spacing": ["warn"]
        }
    }
];
